<?php
/**
 * @author Sergio Hernández Martínez <hm.sergio@gmail.com> 
 * Date: 21/01/2015
 * Time: 11:43
 */

namespace SiteMap;


class SiteMap {

    /**
     * @var string
     */
    private $header;

    /**
     * @var SiteMapElement[]
     */
    private $elements;


    function __construct()
    {
    }

    /**
     * @return bool
     */
    public function hasElements()
    {
        if(count($this->elements) > 0)
            return true;
        else
            return false;
    }

    /**
     * @return SiteMapElement[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param $elements
     * @return $this
     */
    public function setElements($elements)
    {
        $this->elements = $elements;

        return $this;
    }

    /**
     * @param SiteMapElement $element
     * @return $this
     */
    public function addElement(SiteMapElement $element){
        $this->elements[] = $element;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param $header
     * @return $this
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * @return string
     */
    public function getSiteMapXML(){
        $defaultHeader= "<?xml version='1.0' encoding='UTF-8'?>
    <urlset
        xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'
        xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
        xsi:schemaLocation='http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'
        xmlns:image='http://www.google.com/schemas/sitemap-image/1.1'
        >
        ";
        if (isset($this->header)){
            $siteMapXML = $this->header;
        }else{
            $siteMapXML = $defaultHeader;
        }
        if (isset($this->elements)){
            foreach ($this->elements as $element) {
                $url = $element->getUrl();
                $frequency = $element->getChangeFrequency();
                $modified = $element->getLastModified();
                $priority = $element->getPriority();
                $images = $element->getImages();
                $siteMapXML .= "
            <url>";
                if (isset($url)) {
                    $siteMapXML .= "
                    <loc>" . $element->getUrl() . "</loc>
                    ";
                }
                if(isset($frequency)){
                    $siteMapXML .= "
                    <changefreq>".$frequency."</changefreq>
                    ";
                }
                if(isset($modified)){
                    $siteMapXML .= "
                    <lastmod>".$modified->format('Y-m-d H:i:s')."</lastmod>
                    ";
                }
                if(isset($priority)){
                    $siteMapXML .= "
                    <priority>".$priority."</priority>
                    ";
                }
                if(isset($images)){
                    foreach($images as $image){
                        $siteMapXML .= "
                    <image:image>
                        <image:loc>".$image."</image:loc>
                    </image:image>";
                    }
                }
                $siteMapXML .= "
            </url>";
            }
        }else{
            $siteMapXML = 'No hay enlaces definidos.';
        }
        $siteMapXML .= "
    </urlset>";

        return $siteMapXML;
    }
}