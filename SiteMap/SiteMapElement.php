<?php
/**
 * @author Sergio Hernández Martínez <hm.sergio@gmail.com> 
 * Date: 21/01/2015
 * Time: 12:24
 */

namespace SiteMap;


class SiteMapElement {

    /**
     * @var string
     */
    private $url;

    /**
     * @var \DateTime
     */
    private $lastModified;

    /**
     * @var string
     */
    private $changeFrequency;

    /**
     * @var float
     */
    private $priority;

    /**
     * @var string[]
     */
    private $images = array();

    /**
     * @param $url
     * @param null $lastModified
     * @param null $changeFrequency
     * @param null $priority
     * @param array $images
     */
    function __construct($url, $lastModified = null, $changeFrequency = null, $priority = null, $images = array())
    {
        $this->url = $url;
        $this->lastModified = $lastModified;
        $this->changeFrequency = $changeFrequency;
        $this->priority = $priority;
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getChangeFrequency()
    {
        return $this->changeFrequency;
    }

    /**
     * @param string $changeFrequency
     */
    public function setChangeFrequency($changeFrequency)
    {
        $this->changeFrequency = $changeFrequency;
    }

    /**
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * @param \DateTime $lastModified
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;
    }

    /**
     * @return float
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param float $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @param string $url
     */
    public function addImage($url)
    {
        $this->images[] = $url;
    }
}